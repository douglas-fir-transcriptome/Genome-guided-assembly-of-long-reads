## Douglas-fir-long-read-transcriptome

[Step 0 About this experiment](#step-0-about-this-experiment)  
[Step 1 QC with PBBioconda isoseq3 workflow](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Isoseq3-quality-control)  
[Step 2 ID coding regions using transdecoder](#step-2-identify-coding-regions-using-transdecoder)  
[Step 3 Collapse redundant transcripts using vsearch](#step-3-collapse-redundant-transcripts-using-vsearch)  
[Step 4 Align unique CDS transcripts to reference genome using gmap](#step-4-align-cds-transcripts-to-reference-genome-using-gmap)  
[Step 5 Create fasta from gff3 using gFACs](#step-5-create-fasta-from-gff3-using-gfacs)  
[Step 6 Cluster all libraries together using vsearch](#step-6-cluster-all-libraries-together-using-vsearch)  
[Step 7 Assess alignment using busco](#step-7-assess-alignment-using-busco)  
[Step 8 Functional gene annotation for de novo assembly using EnTAP](#step-8-functional-gene-annotation-for-de-novo-assembly-using-entap)  
[Step 9 Evaluate de novo transcriptome assembly and alignment against reference genome using rnaQUAST](#step-9-evaluate-de-novo-transcriptome-assembly-and-alignment-against-reference-genome-using-rnaquast)   

----
### **Step 0** About this experiment

The samples we used for long-read transcriptomics are needles of four Douglas-fir provenances exposed to several summer stress.  These data comes with short-read transcriptomes and is intended for hybrid sequencing.  You can find my workflow for hybrid sequencing [here](https://drive.google.com/open?id=1RN_6JktYG0ozZmovGTyBdkbGEnZ5UFZ5).    

----
### **Step 1** [Quality control using IsoSeq3 workflow](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Isoseq3-quality-control) 

### **Step 2** Identify coding regions using [transdecoder](https://github.com/TransDecoder/TransDecoder/wiki)

**INPUT**  
>   filename.polished.hq.fasta

**Script**: [transdecoder.sh](/transdecoder.sh)  
```
module load hmmer/3.2.1
module load TransDecoder/5.3.0

 TransDecoder.LongOrfs -t filename.polished.hq.fasta

 hmmscan --cpu 16 \
        --domtblout pfam.domtblout \
        /isg/shared/databases/Pfam/Pfam-A.hmm \
        filename.fasta.transdecoder_dir/longest_orfs.pep

 TransDecoder.Predict -t filename.fasta \
        --retain_pfam_hits pfam.domtblout \
        --cpu 16
```

**OUPUT**  
>   filename.transdecoder.cds  
>   filename.transdecoder.gff3  
>   filename.transdecoder.pep  
>   filename.pfam.domtblout   
>   filename.pipeliner.#####.cmds   
>   filename.pipeliner.#####.cmds   
>   filename.pipeliner.#####.cmds   
>   filename.transdecoder_dir  
>   filename.transdecoder_dir.__checkpoints  
>   filename.transdecoder_dir.__checkpoints_longorfs  

[Here](https://docs.google.com/spreadsheets/d/1TyemnlQ7HMIrplc45IAvJChz-jfkXejTn08X5fld4rM/edit?usp=sharing) is the summary of output for transdecoder using my data.  

----
### **Step 3** Collapse redundant transcripts using [**Vsearch**](https://github.com/torognes/vsearch/wiki/Clustering)

**INPUT**    
>   filename.fasta.transdecoder.cds  

**Script** [vsearch1.sh](/vsearch1.sh)  
```
module load vsearch/2.4.3

vsearch --threads 8 --log filename.85_LOGFile \
        --cluster_fast filename.fasta.transdecoder.cds \
        --id 0.95 \
        --centroids filename.95.centroids.fasta \
        --uc filename.95.clusters.uc
```

**OUTPUT**  
>   filename.95.centroids.fasta  
>   filename.95.clusters.uc  
>   filename.95_LOGFile  

[Here](https://docs.google.com/spreadsheets/d/1Kn4lvbEcYrXlDv8Qsv6-6iiHqGZbCtlArlY0fqK1yiI/edit?usp=sharing) is the summary of output for Vsearch using my data.     

----
### **Step 4** Align cds transcripts to reference genome using [**Gmap**](https://github.com/Magdoll/cDNA_Cupcake/wiki/Best-practice-for-aligning-Iso-Seq-to-reference-genome:-minimap2,-deSALT,-GMAP,-STAR,-BLAT#refgmap)

**INPUT**  
>   filename.95.centroids.fasta  
>   psme_v1.0_5000kbpSoftMasked(reference genome)  
 
**Script** [gmap_gff3.sh](/gmap_gff3.sh) 

```
module load gmap/2019-06-10

ln -s /isg/shared/databases/alignerIndex/plant/Psme/gmap/psme_v1.0_5000kbpSoftMasked

gmapl  -K 1000000 -L 10000000 -a 1 --cross-species \
       -D dir/with/psme_v1.0_5000kbpSoftMasked -d psme_v1.0_5000kbpSoftMasked \
       -f gff3_gene filename.95.centroids.fasta \
       --fulllength --nthreads=12 --min-trimmed-coverage=0.95 \ 
       --min-identity=0.92 -n1 > filename.95.centroids.fasta_PSMEv1.0_95_92.gff3 \
        2> filename.95.centroids.fasta_PSMEv1.0_95_92.error
```

**OUTPUT**  
>   filename.95.centroids.fasta_PSMEv1.0_95_92.gff3  
>   filename.95.centroids.fasta_PSMEv1.0_95_92.error  

### **Step 5** Create fasta from gff3 using [**gFACs**](https://gfacs.readthedocs.io/en/latest/Flags/index.html)

**INPUT**  
>   filename.95.centroids.fasta_PSMEv1.0_95_92.gff3   
>   [Psme_v1.0.5000.fasta](/isg/shared/databases/alignerIndex/plant/Psme/genome/v1.0.5000/Psme_v1.0.5000.fasta), reference genome for coastal Douglas-fir  

**Script** [gfacs.sh](/gfacs.sh)
```
module load perl
GFA2="/home/CAM/vvelasco/Applications/gFACs-master"

perl $GFA2/gFACs.pl -f gmap_2017_03_17_gff3 \
                    -p filename \
                    --statistics \
                    --get-fasta-without-introns \
                    --fasta Psme_v1.0.5000.fasta \
                    -O working.directory \
                    filename.95.centroids.fasta_PSMEv1.0_95_92.gff3
 ```     

**OUTPUT**  
>   filename.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta  
>   filename.95.centroids.fasta_PSMEv1.0_95_92_gene_table.txt  
>   filename.95.centroids.fasta_PSMEv1.0_95_92_gFACs_log.txt  
>   filename.95.centroids.fasta_PSMEv1.0_95_92_statistics.txt  

[Here](https://docs.google.com/spreadsheets/d/1gc5E1_YsHGuvXdrNPTCZ7fLrGIcwIQqwCsXaLetEQgg/edit?usp=sharing) is the output for gFACs using my data.  

----
### **Step 6** Cluster all libraries together using [**Vsearch**](https://github.com/torognes/vsearch/wiki/Clustering)  

**INPUT**  
>   filename.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta  (all)  

**Script** [vsearch2.sh](/vsearch2.sh)  

```
cat *.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta > ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta

module load vsearch/2.4.3

vsearch --threads 8 --log ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.LOGFile \
        --cluster_fast ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta \
        --id 0.80 \
        --centroids ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta \
        --uc ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.clusters.uc

```              
 
**OUTPUT**  
>   ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta  
>   ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.clusters.uc  
>   ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.LOGFile  

[Here](https://docs.google.com/spreadsheets/d/1du73UWC68H_F9WJYQNXECWMmkSew2bkJYRBWGFVT7iY/edit?usp=sharing) is the output for vsearch2 using my data.  

----
### **Step 7** Assess alignment using [**BUSCO**](https://busco.ezlab.org/)

**INPUT**  
>   ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta ([Step 6 Vsearch output](#step-6-cluster-all-libraries-together-using-vsearch)) **OR**  
>   filename.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta ([Step 5 gFACs output](#step-5-create-fasta-from-gff3-using-gfacs))  
>   [eukaryote_odb9](https://busco.ezlab.org/datasets/eukaryota_odb9.tar.gz), downloaded and decompressed **OR**  
>   [viridiplantae_odb10](https://busco.ezlab.org/datasets/prerelease/viridiplantae_odb10.tar.gz), downloaded and decompressed  

**Script** [busco.sh](/busco.sh)

```
module load busco/3.0.2b
module load hmmer/3.1b2
module load python/3.6.3
scripts=/isg/shared/apps/busco/3.0.2b/scripts

sed -i 's/\///g' *.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta

python $scripts/run_BUSCO.py -o ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta_euk09 \
                             -i ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta -l eukaryota_odb9 -m tran

python $scripts/run_BUSCO.py -o ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta_vir10 \
                             -i ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta -l viridiplantae_odb10 -m tran

python $scripts/run_BUSCO.py -o filename.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta_euk09 \
                             -i filename.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta -l eukaryota_odb9 -m tran
                             
python $scripts/run_BUSCO.py -o filename.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta_vir10 \
                             -i filename.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta -l viridiplantae_odb10 -m tran

```

**OUTPUT**  
>   run_ALL/   
>   |--blast_output/  
>   |--full_table_filename.tsv   
>   |--hmmer output/  
>   |--missing_busco_list_filename.tsv    
>   |--short_summary_filename.txt   
>   |--translated_proteins/  

[Here](https://docs.google.com/spreadsheets/d/1q0pMy3NlIzQDAJBBF0Zbd6Ez8bhMCaiQv4bs4YvrBjw/edit?usp=sharing) is my busco summary output for my own data.  

----
###  **Step 8** Functional gene annotation for de novo assembly using [**EnTAP**](https://entap.readthedocs.io/en/latest/introduction.html)  
**INPUT**  
>   ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta   ([Step 6 Vsearch output](#step-6-cluster-all-libraries-together-using-vsearch))  
>   /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.92.dmnd  
>   /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd  

**Script** [entap.sh](/entap.sh)   

First, we need to translate cds to protein using [emboss](https://2014-5-metagenomics-workshop.readthedocs.io/en/latest/annotation/translation.html)
```
module load emboss/6.6.0
transeq -sequence ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta -outseq ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.pep.fasta
```

Second we need to check if there are duplicate headers in each file
```
grep ">" ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.pep.fasta | uniq -d
```

To make all headers unique:
```
perl -pe 's/$/_$seen{$_}/ if ++$seen{$_}>1 and /^>/; ' ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.pep.fasta > ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.pep.uniq.fasta
```
Notice that I did not overwrite my fasta file.  I will need to clean this up later.  

Finally, I can run entap

```
module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0

/labs/Wegrzyn/EnTAP/EnTAP --runP \
   -i ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.pep.uniq.fasta   \
   --single-end \
   -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.92.dmnd \
   -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \
   --out-dir ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.pep.uniq_outfiles \
   --ontology 0 \
   --threads 16
```

**OUTPUT**   
>   debug_<date&timecreated>.txt  
>   final_annotations_lvl0.tsv  
>   final_annotations_lvl4_no_contam.tsv  
>   ontology/  
>   |--EggNOG/   
>   |--|--figures/  
>   |--|--processed/   
>   entap_out/  
>   final_annotations_lvl3_contam.tsv   
>   final_annotations_lvl4.tsv       
>   similarity_search/  
>   final_annotated.faa     
>   final_annotations_lvl3_no_contam.tsv   
>   final_unannotated.faa  
>   final_annotations_lvl0_contam.tsv     
>   final_annotations_lvl3.tsv            
>   final_unannotated.fnn  
>   final_annotations_lvl0_no_contam.tsv  
>   final_annotations_lvl4_contam.tsv     
>   log_file_<date&timecreated>.txt  

_ontology/EggNOG/figures/_ contains GO annotation (biological_process, cellular_components, molecular_function bar graphs in png and txt format.  It also contains tax scope which I do not know what it is for.  
_ontology/EggNOG/processed/_ contains annotated and unannotated sequences in faa and fnn format  
_similary_search/_ contain blastp output for your input and database  

The outfiles for my entap run are in /labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/12_Entap/.  
[Here](https://drive.google.com/file/d/1HcHGJWcBfpAnF4S7lAeB3htnIebkr2gl/view?usp=sharing) is the entap output log file.

!!Remove uniq.fasta files after the run?  or the one with duplicates?
----
### **Step 9** Evaluate de novo transcriptome assembly and alignment against reference genome using [**rnaQUAST**](http://cab.spbu.ru/software/rnaquast/)  


