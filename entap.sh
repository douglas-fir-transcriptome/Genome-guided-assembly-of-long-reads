#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

VSE="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/10_Vsearch"
ENT="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/12_Entap"

module load emboss/6.6.0
module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0

cd $ENT

for fname in $VSE/*.fasta; do
  base=$(basename $fname .fasta)
  cp $fname $ENT
  transeq -sequence ${base}.fasta -outseq ${base}.pep.fasta
done

for fname in *.fasta; do
  base=$(basename $fname .fasta)

perl -pe 's/$/_$seen{$_}/ if ++$seen{$_}>1 and /^>/; ' ${fname} > ${base}.1.fasta

sleep 10s

/labs/Wegrzyn/EnTAP/EnTAP --runP \
   -i ${base}.1.fasta \
   --single-end \
   -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.92.dmnd \
   -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \
   --out-dir ${base}.1._outfiles \
   --ontology 0 \
   --threads 16

done

date

