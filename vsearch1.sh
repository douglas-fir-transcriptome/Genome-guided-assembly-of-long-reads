#!/bin/bash
#SBATCH --job-name=vsearch
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

TRA="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/6_Transdecoder"
VSE="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/7_Vsearch"

module load vsearch/2.4.3

for dir in $TRA/*/; do
 cd $dir

 for fname in *.fasta.transdecoder.cds; do

 base=$(basename $fname .fasta.transdecoder.cds)

 vsearch --threads 8 --log ${VSE}/${base}.85_LOGFile \
        --cluster_fast $fname \
        --id 0.85 \
        --centroids $VSE/${base}.85.centroids.fasta \
        --uc $VSE/${base}.85.clusters.uc

done
done


date
