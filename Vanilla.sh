#!/bin/bash
#SBATCH --job-name=Vanilla
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=1G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load R/3.4.3

echo hostname
date

vanilla_r="/home/CAM/vvelasco/barcoding/scripts/r"

cd /labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/3_Lima/

for fname in *.fl.lima.report; do
  base=$(basename $fname .fl.lima.report)

  mkdir $base
  cd $base

  Rscript --vanilla $vanilla_r/report_detail.R $fname pdf
  Rscript --vanilla $vanilla_r/report_summary.R $fname pdf

  cd ..

done

date
