#!/bin/bash
#SBATCH --job-name=cupcake
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hostname
date

SUB="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/1_Subreads"
CCS="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/2_Ccs"
LIM="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/3_Lima"
REF="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/4_Isoseq3_refine"
CLU="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/5_Isoseq3_cluster"

module load isoseq3/3.1.2

for fname in ${SUB}/*subreads.bam*; do
  base=$(basename $fname subreads.bam)

ccs ${fname} ${CCS}/.bam --noPolish --minPasses 1

lima $CCS/${base}.ccs.bam ${CCS}/primers.fasta ${LIM}/${base}.fl.bam --isoseq --no-pbi --dump-clips

isoseq3 refine ${LIM}/${base}.fl.primer_5p--primer_3p.bam ${CCS}/primers.fasta ${REF}/${base}.flnc.bam --require-polya

isoseq3 cluster ${REF}/${base}.flnc.bam ${CLU}/${base}.unpolished.bam --verbose

isoseq3 polish ${CLU}/${base}.unpolished.bam ${SUB}/${base}.subreads.bam ${CLU}/${base}.polished.bam --verbose

isoseq3 summarize ${CLU}/${base}.polished.bam ${CLU}/${base}.summary.csv --verbose;

done

date

