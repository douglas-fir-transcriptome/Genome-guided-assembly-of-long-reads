#!/bin/bash
#SBATCH --job-name=transdecoder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hostname
date

module load hmmer/3.2.1
module load TransDecoder/5.3.0

CLU="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/5_Isoseq3_cluster"
TRA="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/6_Transdecoder"

for fname in $CLU/*.polished.hq.fasta; do
  base=$(basename $fname .polished.hq.fasta)
  
 ln -s $fname $TRA/${base}.fasta

 cd $TRA

 mkdir ${base}_1
 cd ${base}_1

 TransDecoder.LongOrfs -t ${base}.fasta

 hmmscan --cpu 16 \
        --domtblout pfam.domtblout \
        /isg/shared/databases/Pfam/Pfam-A.hmm \
        ${base}.fasta.transdecoder_dir/longest_orfs.pep

 TransDecoder.Predict -t ${base}.fasta \
        --retain_pfam_hits pfam.domtblout \
        --cpu 16
       
 cd ..
 
done

date
