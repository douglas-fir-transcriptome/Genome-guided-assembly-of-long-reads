#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hostname
date

GMA="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/8_Gmap"
GEN="/isg/shared/databases/alignerIndex/plant/Psme/genome/v1.0.5000/Psme_v1.0.5000.fasta"
GFA1="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/9_Gfacs"
GFA2="/home/CAM/vvelasco/Applications/gFACs-master"

module load perl

for fname in $GMA/*.gff3; do
  base=$(basename $fname .gff3)

perl $GFA2/gFACs.pl -f gmap_2017_03_17_gff3 \
                    -p ${base} \
                    --statistics \
                    --get-fasta-without-introns \
                    --fasta $GEN \
                    -O $GFA1 \
                    $fname

done

date
