#!/bin/bash
#SBATCH --job-name=vsearch
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

GMA="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/8_Gmap"
VSE="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/10_Vsearch"

cd $GFA

cat *.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta > ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta

sleep 1m

module load vsearch/2.4.3

cd $VSE

vsearch --threads 8 --log ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.LOGFile \
        --cluster_fast $GFA/ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta \
        --id 0.80 \
        --centroids ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta \
        --uc ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.clusters.uc

date

