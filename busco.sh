#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hostname
date

GMA="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/8_Gmap"
GFA="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/9_Gfacs"
VSE="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/10_Vsearch"
BUS="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/11_Busco"

scripts=/isg/shared/apps/busco/3.0.2b/scripts

cd $BUS

module load busco/3.0.2b
module load hmmer/3.1b2
module load python/3.6.3

sleep 1m

sed -i 's/\///g' ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta

python $scripts/run_BUSCO.py -o ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta_euk09 \
                             -i $VSE/ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta -l eukaryota_odb9 -m tran

python $scripts/run_BUSCO.py -o ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta_vir10 \
                             -i $VSE/ALL.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta.80.centroids.fasta -l viridiplantae_odb10 -m tran

sleep 1m

for fname in $GFA/*.95.centroids.fasta_PSMEv1.0_95_92_genes_without_introns.fasta; do

 base=$(basename $fname)

 sed -i 's/\///g' $fname

 python $scripts/run_BUSCO.py -o ${base}_euk09 -i $fname -l eukaryota_odb9 -m tran

 python $scripts/run_BUSCO.py -o ${base}_vir10 -i $fname -l viridiplantae_odb10 -m tran

done

date