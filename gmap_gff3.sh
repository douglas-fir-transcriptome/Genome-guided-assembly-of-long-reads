#!/bin/bash
#SBATCH --job-name=gmapl
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hostname
date

VSE="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/7_Vsearch"
GMA="/labs/Wegrzyn/CoAdapTree_Douglasfir/IsoSeq/8_Gmap"

module load gmap/2019-06-10

cd $GMA

#ln -s /isg/shared/databases/alignerIndex/plant/Psme/gmap/psme_v1.0_5000kbpSoftMasked

for fname in $VSE/*.95.centroids.fasta; do
   base=$(basename $fname)

 gmapl  -K 1000000 -L 10000000 -a 1 --cross-species \
        -D $GMA -d psme_v1.0_5000kbpSoftMasked  -f gff3_gene ${fname} \
        --fulllength --nthreads=12 --min-trimmed-coverage=0.95 --min-identity=0.92 -n1 > ${base}_PSMEv1.0_95_92.gff3 \
        2> ${base}_PSMEv1.0_95_92.error

done

